import React, { useState } from "react";
import DatePicker from "react-datepicker";
import ReactDOM from "react-dom";

import "react-datepicker/dist/react-datepicker.css";

const TaskCard = ({
  mode,
  id,
  task_type,
  name,
  importance,
  time_to_complete,
  density,
  activation_time,
}) => {
  const isModeSet = () => mode !== "None" || !mode;
  const getDefaultActivationTime = () => {
    if (activation_time && Date.parse(activation_time)) {
      return new Date(activation_time);
    }
    return null;
  };

  const isNewTaskForm = () => !id;

  const [activationTime, setActivationTime] = useState(
    getDefaultActivationTime()
  );

  const formAction = isNewTaskForm()
    ? "/api/form/tasks"
    : `/api/form/tasks/${id}/update`;

  const isDeactivated = () => density === "0";

  const taskCardStyle = () => {
    if (isNewTaskForm()) {
      return "w3-green";
    }

    if (isDeactivated()) {
      return "w3-gray";
    }

    return "";
  };

  const taskTypeSection = () => (
    <>
      <select
        name="task_type"
        disabled={isModeSet()}
        defaultValue={task_type}
        className="w3-select"
      >
        <option value="home">Home</option>
        <option value="work">Work</option>
      </select>
      <label>Task Type</label>
      {isModeSet() && <input type="hidden" name="task_type" value={mode} />}
    </>
  );

  const densitySection = () => {
    if (isNewTaskForm()) {
      return null;
    }

    const densityTipMessage = isDeactivated() ? (
      <strong>(Task may not be activated yet)</strong>
    ) : (
      ""
    );

    return (
      <>
        <input
          type="number"
          defaultValue={parseFloat(density).toFixed(3)}
          className="w3-input"
          readOnly
        />
        <label>Density {densityTipMessage}</label>
      </>
    );
  };

  const activationTimeSection = () => (
    <>
      <DatePicker
        className="w3-input"
        selected={activationTime}
        onChange={(date) => setActivationTime(date)}
        dateFormat="MMMM d, yyyy h:mm aa"
        showTimeSelect
      />
      <input
        type="hidden"
        name="activation_time"
        value={(activationTime && activationTime.toISOString()) || ""}
      />
      <label>Activation Time</label>
    </>
  );

  const deleteButton = () => {
    if (isNewTaskForm()) {
      return null;
    }

    return (
      <form
        action={`/api/form/tasks/${id}/delete`}
        method="post"
        onSubmit={() => confirm("Do you want to delete?")}
      >
        <input type="submit" value="Delete" className="w3-input w3-red" />
      </form>
    );
  };

  return (
    <div className={`w3-card-4 w3-container w3-padding-16 ${taskCardStyle()}`}>
      <form action={formAction} method="post">
        <input
          type="text"
          name="name"
          defaultValue={name}
          size="40"
          className="w3-input"
        />
        <label>Name</label>
        {taskTypeSection()}
        <input
          type="number"
          name="importance"
          min="0.5"
          defaultValue={importance}
          className="w3-input"
          step="0.5"
        />
        <label>Importance</label>
        <input
          type="number"
          name="time_to_complete"
          min="1"
          defaultValue={time_to_complete}
          className="w3-input"
        />
        <label>Time To Complete</label>
        {activationTimeSection()}
        {densitySection()}
        <input
          type="submit"
          value={isNewTaskForm() ? "Create Task" : "Update"}
          className="w3-input w3-blue-gray"
        />
      </form>
      <button
        className="w3-input w3-black"
        onClick={() => setActivationTime(null)}
      >
        Remove activation time
      </button>
      {deleteButton()}
    </div>
  );
};

const elements = document.getElementsByClassName("task-card");

for (let i = 0; i < elements.length; i++) {
  const el = elements.item(i);
  ReactDOM.render(<TaskCard {...el.dataset} />, el);
}
