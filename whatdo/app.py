import secrets

from fastapi import Depends, FastAPI, HTTPException, status, APIRouter
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi.staticfiles import StaticFiles

from whatdo.config import config
from whatdo.controller import add_controllers


def create_app():
    security = HTTPBasic()

    def basic_auth(credentials: HTTPBasicCredentials = Depends(security)):
        correct_username = secrets.compare_digest(credentials.username, "admin")
        correct_password = secrets.compare_digest(
            credentials.password, config.USER_PASSWORD
        )

        if not (correct_username and correct_password):
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Incorrect email or password",
                headers={"WWW-Authenticate": "Basic"},
            )

    app = FastAPI(dependencies=[Depends(basic_auth)])
    app.mount("/dist", StaticFiles(directory="dist"), name="dist")
    app.mount("/local-storage", StaticFiles(directory="ui/build", html=True), name="ui-build")

    secure_router = APIRouter(dependencies=[Depends(basic_auth)])
    add_controllers(secure_router)

    app.include_router(secure_router)

    return app


APP = create_app()
