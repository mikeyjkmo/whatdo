from typing import List

from motor.motor_asyncio import AsyncIOMotorClient
from pydantic.types import UUID4

from whatdo.schemas import Task
from whatdo.config import config


class TaskRepository:
    tasks: List[Task]

    def __init__(self):
        self.tasks = [
            Task(name="blah", importance=5, time_to_complete=10),
            Task(name="The most important thing", importance=8, time_to_complete=10),
            Task(name="blah", importance=5, time_to_complete=10),
        ]

    async def list(self, task_type="home"):
        return [
            t for t in sorted(self.tasks, key=lambda t: t.density, reverse=True)
            if task_type is not None and t.task_type == task_type
        ]

    async def add(self, task):
        self.tasks.append(task)

    async def remove(self, task_id: UUID4):
        self.tasks = [t for t in self.tasks if t.id != task_id]


class MongoTaskRepository:
    def __init__(self):
        self._db = AsyncIOMotorClient(config.MONGO_CONNECTION_STR)["whatdo"]

    async def list(self, task_type=None):
        filters = {}

        if task_type:
            filters["task_type"] = task_type

        cursor = self._db.tasks.find(filters).sort('density', -1)
        return [Task.parse_obj(t) for t in await cursor.to_list(length=None)]

    async def add(self, task):
        await self._db.tasks.insert_one(task.dict())

    async def remove(self, task_id: UUID4):
        await self._db.tasks.delete_one({"id": task_id})

    async def update(self, task_id: UUID4, updated_task: Task):
        updated_task_details = updated_task.dict()
        del updated_task_details["id"]
        await self._db.tasks.update_one({"id": task_id}, {"$set": updated_task_details})
