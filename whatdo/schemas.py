from datetime import datetime, timezone
from typing import Literal, Optional
from uuid import uuid4

from pydantic import BaseModel, root_validator
from pydantic.types import UUID4


class Task(BaseModel):
    name: str
    importance: float
    time_to_complete: int
    density: Optional[float]
    id: Optional[UUID4]
    task_type: Literal["home", "work"] = "home"
    activation_time: Optional[datetime]

    @root_validator
    def calculate_density(cls, values):
        if values.get("density") is None:
            values["density"] = values["importance"] / values["time_to_complete"]
        return values

    @root_validator
    def set_id(cls, values):
        if not values["id"]:
            values["id"] = uuid4()
        return values

    def dict(self):
        result = super().dict()
        result["activation_time"]: datetime = (
            self.activation_time.isoformat() if self.activation_time else None
        )
        return result

    @property
    def effective_density(self):
        if self.activation_time and self.activation_time > datetime.now(
            tz=timezone.utc
        ):
            return 0
        return self.density
