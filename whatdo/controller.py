from datetime import datetime
from typing import Literal, Optional

from fastapi import Form, Request, Cookie, status
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates
from pydantic.types import UUID4

from whatdo.schemas import Task
from whatdo.task_repository import MongoTaskRepository, TaskRepository


def add_form_api(app, task_repository: TaskRepository):
    @app.post("/api/form/tasks")
    async def form_create_task(
        name: str = Form(...),
        task_type: Literal["home", "work"] = Form(...),
        importance: float = Form(...),
        time_to_complete: int = Form(...),
        activation_time: Optional[datetime] = Form(default=None),
    ):
        new_task = Task(
            name=name,
            task_type=task_type,
            importance=importance,
            time_to_complete=time_to_complete,
            activation_time=activation_time,
        )
        await task_repository.add(new_task)

        return RedirectResponse("/", status_code=status.HTTP_302_FOUND)

    @app.post("/api/form/tasks/{task_id}/delete")
    async def form_delete_task(task_id: UUID4):
        await task_repository.remove(task_id)
        return RedirectResponse("/", status_code=status.HTTP_302_FOUND)

    @app.post("/api/form/tasks/{task_id}/update")
    async def form_update_task(
        task_id: UUID4,
        name: str = Form(...),
        task_type: Literal["home", "work"] = Form(...),
        importance: float = Form(...),
        time_to_complete: int = Form(...),
        activation_time: Optional[datetime] = Form(default=None),
    ):
        updated_task = Task(
            id=task_id,
            name=name,
            task_type=task_type,
            importance=importance,
            time_to_complete=time_to_complete,
            activation_time=activation_time,
        )
        await task_repository.update(
            task_id,
            updated_task,
        )
        return RedirectResponse("/", status_code=status.HTTP_302_FOUND)

    @app.post("/api/form/mode/set")
    async def set_mode_cookie(mode: Literal["home", "work", "none"] = Form(...)):
        response = RedirectResponse("/", status_code=status.HTTP_302_FOUND)

        if mode == "none":
            response.delete_cookie(key="mode")
        else:
            response.set_cookie(key="mode", value=mode)

        return response


def prepate_task_for_display(task):
    return {**task.dict(), "effective_density": task.effective_density}


def prepare_tasks_for_display(tasks):
    result = [prepate_task_for_display(t) for t in tasks]
    return sorted(result, key=lambda t: t["effective_density"], reverse=True)


def add_controllers(app):
    templates = Jinja2Templates(directory="whatdo/templates")
    task_repository = MongoTaskRepository()

    @app.get("/", response_class=HTMLResponse)
    async def index(
        request: Request,
        mode: Optional[str] = Cookie(None),
    ):
        tasks = await task_repository.list(task_type=mode)
        return templates.TemplateResponse(
            "index.j2",
            context={
                "request": request,
                "tasks": prepare_tasks_for_display(tasks),
                "mode": mode,
            },
        )

    @app.get("/ping")
    async def ping():
        return "pong"

    add_form_api(app, task_repository)
