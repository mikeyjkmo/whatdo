from pydantic import BaseSettings


class WhatDoConfig(BaseSettings):
    MONGO_CONNECTION_STR: str
    USER_PASSWORD: str


config = WhatDoConfig()
