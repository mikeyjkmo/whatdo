FROM python:3.8-alpine AS base

RUN apk add --no-cache \
  gcc \
  musl-dev \
  make \
  npm

COPY . $HOME/whatdo

WORKDIR $HOME/whatdo
RUN pip install .
RUN npm install
RUN make compile

WORKDIR $HOME/whatdo/ui
RUN npm install && npm run build

WORKDIR $HOME/whatdo
ENV PORT=8080

ENTRYPOINT ["make run"]
