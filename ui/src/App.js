import { useState, useEffect } from "react";
import { Button, Card, Col, Divider, Row } from "antd";
import "antd/dist/antd.css";

import {
  Badge,
  Modal,
  Statistic,
  Input,
  Form,
  InputNumber,
  DatePicker,
} from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  CheckCircleOutlined,
} from "@ant-design/icons";

import TaskStore from "./task-store";
import service from "./whatdo-service";
import "./App.css";

const TaskCard = (task) => {
  const {
    id,
    index,
    name,
    importance,
    density,
    timeToComplete,
    activationTime,
  } = task;
  const [pendingDelete, setPendingDelete] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const cardStyle = index === 0 ? { backgroundColor: "#dfeeff" } : {};

  return (
    <Col lg={8} sm={24} xs={24}>
      <Modal
        visible={pendingDelete}
        onCancel={() => setPendingDelete(false)}
        onOk={() => {
          service.delete(id);
          setPendingDelete(false);
        }}
        title="Confirm"
      >
        Discard or complete this task?
      </Modal>
      <TaskFormModal
        visible={isEditing}
        existingTask={task}
        onCancel={() => setIsEditing(false)}
      />
      <Badge.Ribbon text="Work">
        <Card
          title={name}
          className="task-card"
          actions={[
            <CheckCircleOutlined key="mark as done" onClick={() => setPendingDelete(true)} />,
            <EditOutlined key="edit" onClick={() => setIsEditing(true)} />,
            <DeleteOutlined
              key="delete"
              onClick={() => setPendingDelete(true)}
            />,
          ]}
          headStyle={{ fontSize: "1.75em" }}
          style={cardStyle}
        >
          <Row gutter={[16, 16]}>
            <Col>
              <Statistic
                value={importance}
                title="Importance"
                valueStyle={{ color: "#3f8600" }}
              />
            </Col>
            <Col>
              <Statistic
                value={timeToComplete}
                title="Time"
                valueStyle={{ color: "#2222aa" }}
              />
            </Col>
            <Col>
              <Statistic
                value={density}
                title="Density"
                valueStyle={{ color: "#dd0000" }}
              />
            </Col>
            <Col span={24}>
              <Statistic
                value={activationTime || "N/A"}
                title="Activation Time"
                valueStyle={{ color: "#dd0000" }}
              />
            </Col>
          </Row>
        </Card>
      </Badge.Ribbon>
    </Col>
  );
};

function TaskFormModal({ visible, onCancel, existingTask }) {
  return (
    <Modal
      visible={visible}
      onCancel={onCancel}
      title={!existingTask ? "Create a new task" : "Edit task"}
      footer={null}
    >
      <Form
        initialValues={
          existingTask || {
            name: "Take out the bin",
            importance: 5,
            timeToComplete: 5,
            activationTime: null,
          }
        }
        onFinish={(values) => {
          if (existingTask) {
            service.update(existingTask.id, values);
          } else {
            service.add(values);
          }
          onCancel();
        }}
      >
        <Form.Item
          label="Task Name"
          name="name"
          rules={[{ required: true, type: "string" }]}
        >
          <Input placeholder="Take out the bin" />
        </Form.Item>
        <Form.Item
          label="Importance"
          name="importance"
          rules={[
            {
              required: true,
              type: "number",
              message:
                "Importance must be a number between 1 and 10 (inclusive)",
            },
          ]}
        >
          <InputNumber max={10} min={0} step={1} />
        </Form.Item>
        <Form.Item
          label="Time To Complete"
          name="timeToComplete"
          rules={[{ required: true, type: "number" }]}
        >
          <InputNumber min={1} step={1} addonAfter="mins" />
        </Form.Item>
        <Form.Item label="Activation Time (Optional)" name="activationTime">
          <DatePicker showTime />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            {
              existingTask ? "Edit" : "Create"
            }
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
}

const localStoragePersistence = () => {
  const existingTasks = localStorage.getItem("tasks");
  if (existingTasks) {
    service.load(JSON.parse(existingTasks));
  }

  const unsubscribeFromTaskStore = TaskStore.subscribe(
    s => s.tasks,
    newTasks => {
      localStorage.setItem("tasks", JSON.stringify(newTasks));
    }
  );

  return () => {
    unsubscribeFromTaskStore();
  };
}

function App() {
  const tasks = TaskStore.useState((s) => s.tasks);
  const [creatingNewTask, setCreatingNewTask] = useState(false);

  useEffect(localStoragePersistence, []);

  return (
    <div className="App">
      <TaskFormModal
        visible={creatingNewTask}
        onCancel={() => setCreatingNewTask(false)}
      />
      <Row gutter={[16, 16]} justify="end">
        <Col>
          <Button
            type="primary"
            size="large"
            onClick={() => setCreatingNewTask(true)}
          >
            New Task
          </Button>
        </Col>
      </Row>
      <Divider />
      <Row gutter={[16, 16]}>
        <>
          {tasks.map((task, index) => (
            <TaskCard {...{ ...task, index }} />
          ))}
        </>
      </Row>
    </div>
  );
}

export default App;
