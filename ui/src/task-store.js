import { Store } from "pullstate";

export default new Store({
  tasks: [
    {
      id: "5f2a9eae-9161-49d9-a141-6ec9430ad1bf",
      name: "This is the top task",
      importance: 8.0,
      density: 5.0,
      timeToComplete: 30,
      activationTime: null,
    },
    {
      id: "b19102c6-3a7f-430c-8ea3-f2171c31c7b6",
      name: "This is another task",
      importance: 8.0,
      density: 5.0,
      timeToComplete: 30,
      activationTime: null,
    },
  ],
});
