import { v4 as uuidv4 } from "uuid";

import TaskStore from "./task-store";

class WhatdoService {
  async load(tasks) {
    TaskStore.update((s) => {
      s.tasks = tasks;
    });
  }

  async add(task) {
    TaskStore.update((s) => {
      const newTask = {
        ...task,
        id: uuidv4(),
        density: task.importance / task.timeToComplete,
      };
      const tasks = [...s.tasks, newTask];
      tasks.sort((a, b) => b.density - a.density);
      s.tasks = tasks;
    });
  }

  async update(id, task) {
    TaskStore.update((s) => {
      let tasks = s.tasks.filter((t) => t.id !== id);
      const updatedTask = {
        ...task,
        density: task.importance / task.timeToComplete,
      };
      tasks = [...tasks, updatedTask];
      tasks.sort((a, b) => b.density - a.density);
      s.tasks = tasks;
    });
  }

  async delete(id) {
    TaskStore.update((s) => {
      s.tasks = s.tasks.filter((t) => t.id !== id);
    });
  }
}

export default new WhatdoService();
