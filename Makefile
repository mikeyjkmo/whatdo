dev:
	poetry run uvicorn whatdo.app:APP --reload --host 0.0.0.0

dev-js:
	npx webpack build --config ./webpack.config.js --mode development --watch

compile:
	npx webpack build --config ./webpack.config.js --mode production

run:
	uvicorn whatdo.app:APP --port ${PORT} --host 0.0.0.0

docker-build:
	docker build . -t whatdo:latest

.PHONY: \
	dev \
	dev-js \
	compile \
	run \
	docker-build
